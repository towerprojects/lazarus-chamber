**DESCRIPTION**
Lazarus Chamber is a backup solution that requires relatively significant setup, but is rock-solid afterwards. It uses Ansible Tower (or AWX, of course) to backup machines to a target backup server using rsync. The backup process is as follows:
 - Log in to the backup server using existing credentials
 - Generate a one-time use SSH key
 - Allow the one-time SSH key access to specified user
 - Restrict one-time SSH key to specific IP range
 - Pull the one-time key from the backup server
 - Send the one-time key to all of the backup targets
 - Using the new key, use rsync to backup the target machine
 - Delete the one-time keys

**USAGE**
The backuptargets need to be included in the inventory that you're using, tagged with 'BackupHost', the playbook will push the backup to all of the hosts in a loop.
You probably need to set some variables in your tower instance, these can be considered reasonably safe defaults:
```
targets: all
defaultpath: /mnt/backup
backupuser: root
tempidalgorithm: rsa
exclusionspath: /tmp/lazarus_exclude
localiddir: /tmp
localidname: lazarus_id
remoteidpath: "/root/.ssh/id_{{ tempidalgorithm }}"
exclusions:
  - /mnt/*
  - /media/*
  - /lost+found/
  - /var/lib/libvirt/images/*
  - /var/lib/docker/volumes/*
  - /var/lib/docker/overlay2/*
  - /var/lib/docker/tmp/*
```
