#!/usr/bin/python3
# Script to backup selected folders, made for lazarus-chamber
# By Trent Arcuri, 2019
import argparse
import time
import distutils.spawn
import socket
import subprocess
import os

# Gather command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name',
                    type=str, dest='backupname', default=socket.gethostname(),
                    help='Specify name for this backup')
parser.add_argument('-t', '--target',
                    type=str, dest='backuptarget', default='/',
                    help='Specify what to backup')
parser.add_argument('-d', '--directory',
                    type=str, dest='backupdir', default='/mnt/backup/',
                    help='Specify where to backup to')
parser.add_argument('-e', '--exclude-file',
                    type=str, dest='excludefile',
                    help='Specify where to source an exclude file')
args = parser.parse_args()
# Format/shorten arguments into sane variables
# Rsync copies here in the end
finalpath = args.backupdir + args.backupname + '/'
# When the script was called
launchtime = time.time()
humanlaunchtime = time.strftime("%Y-%m-%d %H:%M:%S")
# Rsync command sanitization
rsync = distutils.spawn.find_executable('rsync')
rsyncexcludearg = '--exclude='
rsyncoptions = ['-Pauqr',
                '--delete',
                '--exclude-from=' + args.excludefile]
exclude = ["/dev/*",
           "/proc/*",
           "/sys/*",
           "/tmp/*",
           "/run/*"]
# Add rsyncexcludearg before every exclude listed above
rsyncexclude = [rsyncexcludearg + excludeitem for excludeitem in exclude]
rsyncargs = rsyncoptions + rsyncexclude
rsynccommand = [rsync] + rsyncargs + [args.backuptarget, finalpath]


def banner():
    # Print some info about what is going to run
    print("Backup running at", humanlaunchtime, "\n",
          "Backup target:", args.backuptarget, "\n",
          "Output directory:", args.backupdir, "\n",
          "Backup name:", args.backupname, "\n")


def rsyncbackup():
    # Before backing up, update target modify, access, and change time
    os.utime(args.backuptarget, (launchtime, launchtime))
    # Run the backup process here, print command for log usage/debugging
    print("Running", rsynccommand)
    subprocess.call(rsynccommand)


def main():
    # Main program entry point, call preliminary functions
    banner()
    rsyncbackup()


main()  # Call main function
